(in-package :marvin-web-app)


(defwidget application ()
  ((header :initarg :header
	   :reader header)
   (stack :initarg :stack
	  :reader stack)
   (widget-shop :initarg :widget-shop
		:reader widget-shop)
   (footer :initarg :footer
	   :reader footer)))

(defun current-canvas ()
  (car (stack (weblocks/widgets/root:get))))

(defmethod weblocks/dependencies:get-dependencies ((app application))
  (flet ((make-rel (path)
	   (merge-pathnames
	    (pathname path)
	    (namestring (asdf:system-source-directory :marvin-web-app)))))
    (list
     (weblocks/dependencies:make-dependency (make-rel "assets/bootstrap.min.css"))
     (weblocks/dependencies:make-dependency (make-rel "assets/jquery-3.4.1.min.js"))
     (weblocks/dependencies:make-dependency (make-rel "assets/popper.min.js"))
     (weblocks/dependencies:make-dependency (make-rel "assets/bootstrap.min.js"))

     (weblocks/dependencies:make-dependency (make-rel "assets/jquery-ui.css"))
     (weblocks/dependencies:make-dependency (make-rel "assets/jquery-ui.js"))

     (weblocks/dependencies:make-dependency (make-rel "assets/Chart.min.css"))
     (weblocks/dependencies:make-dependency (make-rel "assets/Chart.min.js")))))

(let ((dummy (make-instance 'dependency-holder)))
  (defmethod render ((app application))
    (with-html
      (inject-dependencies :before dummy)

      (render (header app))
      (render (car (stack app)))
      (render (footer app))

      (inject-dependencies :after dummy))))
