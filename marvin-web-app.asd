;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-


(defsystem :marvin-web-app
  :name "marvin-web-app"
  :description ""
  :version "0.0.3"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "LLGPG"
  :depends-on (:weblocks
	       :weblocks-ui
	       :weblocks-file-server
	       :weblocks-lass
	       :weblocks-websocket
	       :parenscript
	       :water
	       :trivial-open-browser
	       :trivial-json-codec
	       :trivial-object-lock
	       :piggyback-parameters
	       :communication-protocol
	       :log4cl
	       :drakma)
  :components ((:file "package")
	       (:file "common")
	       (:file "weblocks-extension")
	       (:file "application")
	       (:file "widgets/widget-shop")
	       (:file "widgets/canvas")
	       (:file "widgets/droppable-canvas")
	       (:file "widgets/widget-base")
	       (:file "widgets/header-widget")
	       (:file "widgets/footer-widget")
	       (:file "widgets/commission-widget")
	       (:file "marvin-web-app")
	       (:file "widgets/page-widget")
	       (:file "widgets/button-widget")
	       (:file "widgets/table")))
