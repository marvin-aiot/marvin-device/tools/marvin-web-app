(in-package :marvin-web-app)

(defwidget canvas (dependency-holder weblocks/widgets/base:widget)
  ((parent :initarg :parent
	   :reader parent)
   (elements :initarg :elements
	     :initform nil
	     :accessor elements)))

(defmethod get-css-classes ((widget canvas))
  (append '(:canvas)
	  (call-next-method)))

(defmethod render ((this canvas))
  (with-html
    ;; When (> (length (stack (weblocks/widgets/root:get))) 1) also draw a 'back' button (arrow) on the top left corner, which calls (pop-canvas).
    (when (> (length (stack (weblocks/widgets/root:get))) 1)
      (:div :style (format nil "position: absolute; top: ~a; left: 0px;" (widget-height (header (weblocks/widgets/root:get))))
	    (:a :href "#back" :onclick (make-js-action #'pop-canvas)
		(:img :src "assets/img/back_arrow.png" :style "width: 3em;height: 3em; opacity: 20%;"))))))

(defmethod weblocks/dependencies:get-dependencies ((this canvas))
  (list (create-lass-dependency `((".canvas"
				   :margin-top ,(widget-height (header (weblocks/widgets/root:get)))
				   :min-height ,(format nil "calc(100vh - (~a + ~a))"
							(widget-height (header (weblocks/widgets/root:get)))
							(widget-height (footer (weblocks/widgets/root:get)))))))))

(defmethod weblocks/widgets/base:update :after ((this canvas) &key inserted-after inserted-before)
  (declare (ignore inserted-after inserted-before))
  (loop for elm in (elements this)
	do (update elm)))
