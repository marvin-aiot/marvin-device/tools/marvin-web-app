(in-package :marvin-web-app)

(defwidget widget-base (dependency-holder weblocks/widgets/base:widget)
  ((uid :initarg :uid
	:initform (make-uid)
	:reader widget-uid)
   (position :initarg :position
	     :initform (cons 0 0)
	     :accessor widget-position)
   (size :initarg :size
	 :initform nil
	 :accessor widget-size)
   (description :initarg :description
		:initform "<empty>"
		:accessor widget-description)
   (config-mode :initform nil
		:accessor widget-config-mode
		:type boolean)
   (active :initform nil
	   :reader widget-active)
   (accepts-click :initarg :accepts-click
		  :initform t
		  :reader widget-accepts-click)
   (description-ontop :initarg :description-ontop
		      :initform nil
		      :reader widget-description-ontop)
   (parent :initarg :parent
	   :initform (error "A parent elelment must be given.")
	   :reader widget-parent)))

(defgeneric start-widget (widget))
(defgeneric resume-widget (widget))
(defgeneric pause-widget (widget))
(defgeneric stop-widget (widget))
(defgeneric render-widget (widget))
(defgeneric render-configuration (widget))

(defgeneric on-widget-clicked (widget))

(defmethod start-widget ((widget widget-base)))
(defmethod resume-widget ((widget widget-base)))
(defmethod pause-widget ((widget widget-base)))
(defmethod stop-widget ((widget widget-base)))

(defmethod start-widget :after ((widget widget-base))
  (setf (slot-value widget 'active) t))

(defmethod resume-widget :after ((widget widget-base))
  (setf (slot-value widget 'active) t))

(defmethod pause-widget :after ((widget widget-base))
  (setf (slot-value widget 'active) nil))

(defmethod stop-widget :after ((widget widget-base))
  (setf (slot-value widget 'active) nil))

(defmethod on-widget-clicked ((widget widget-base))
  (log:info "Default behaviour is to do nothing..."))

(defmethod render-widget ((widget widget-base))
  )

(defmethod render-configuration ((widget t))
  )

(defmethod render-configuration ((widget widget-base))
  (with-html
    (:div :class "mb-3 row g-3 align-items-end"
	  (:div :class "col-auto" (:label :for "widget-description" :class "form-label" "Description"))
	  (:div :class "col-auto" (:input :type "text" :class "form-control"
					  :id "widget-description" :aria-describedby "widget-description-help"
					  :placeholder "A short and expressive description of the Widget."
					  :value (widget-description widget))))))

(defmethod render-configuration :around ((widget widget-base))
  (with-html
    (:form :class "widget-configuration"
	   (:div :class "config-contents"
		 (call-next-method))
	   (:div :class "config-buttons"
		 (:button :type "button" :class "btn btn-warning" :onclick (make-js-action #'(lambda ()
											       (setf (widget-config-mode widget) nil)
											       (update widget)))
			  "Cancel")
		 (:button :type "button" :class "btn btn-success" :onclick (make-js-action #'(lambda ()
											       (setf (widget-config-mode widget) nil)
											       (update widget)))
			  "Save")))))

(defmethod get-css-classes ((widget widget-base))
  (append '(:widget-base)
	  (call-next-method)))

(defmethod render ((widget widget-base))
  (when (widget-config-mode widget)
    (return-from render (render-configuration widget)))

  (with-html
    (if (widget-accepts-click widget)
	(:a :href "#"
	    :onclick (ps (let ((w (getprop ($ (lisp (concatenate 'string "#" (weblocks/widgets/dom:dom-id widget)))) 0)))
			   (unless (and (chain w class-list)
					(chain w class-list (contains "edit-mode")))
			     (initiate-action (lisp (weblocks/actions:make-action #'(lambda () (on-widget-clicked widget)))))))
			 nil)
	    (render-widget widget))
	(render-widget widget))
    (:div :class "widget-edit-extras invisible-widget-edit-extras"
	  (:a :href "#"
	      :onclick (make-js-action #'(lambda ()
					   (setf (widget-config-mode widget) t)
					   (update widget)))
	      (:img :src "assets/img/widget_edit.png" :style "float: left;width: 20px;height: 21px;"))
	  (:a :href "#"
	      :onclick (make-js-action #'(lambda ()
					   (setf (elements (current-canvas)) (delete widget (elements (current-canvas))))
					   (update (current-canvas))))
	      (:img :src "assets/img/widget_remove.png" :style "float: right;width: 20px;height: 21px;")))))

(defmethod get-inline-style ((widget widget-base))
  (concatenate 'string
	       (format nil "position: absolute; top: ~apx; left: ~apx;"
		       (first (widget-position widget))
		       (second (widget-position widget)))

	       (if (or (not (widget-size widget))
		       (widget-config-mode widget))
		   "" ;; Don't set size while in configuration mode or when size not set
		   (format nil "width: ~apx; height: ~apx;"
			   (first (widget-size widget))
			   (second (widget-size widget))))

	       (call-next-method)))

(defmethod weblocks/dependencies:get-dependencies ((widget widget-base))
  (append (list
	   (create-lass-dependency `((.widget-base
				      :padding 15px
				      :text-align center
				      :border-radius 10px
				      :border-width 1px
				      :border-style hidden
				      (.invisible-widget-edit-extras
				       :display none)
				      (.widget-edit-extras
				       :top -120px
				       :position relative)
				     (.edit-mode
				      :border-style dashed)
				     (.widget-configuration
				      :border-style double
				      :border-radius 1em
				      :padding 0.75em)
				     (.form-text
				      :color grey
				      :font-size small)))))
	  (call-next-method)))

(defmethod initialize-instance :after ((this widget-base) &key)
  )



#|

Widgets are saved as p-lists under the section :widgets

The initial canvas is stored under :initial-canvas and holds the uid of every widget on that page.

|#

(defun update-widget (id &key top left width height description)
  (let ((widget (find id (elements (current-canvas)) :key #'weblocks/widgets/dom:dom-id :test #'string-equal)))
    (if widget
	(progn
	  (when top
	    (setf (first (widget-position widget)) top))
	  (when left
	    (setf (second (widget-position widget)) left))
	  (when width
	    (setf (first (widget-size widget)) width))
	  (when height
	    (setf (second (widget-size widget)) height))
	  (when description
	    (setf (widget-description widget) description)))
	(log:warn "No widget could be found with the id '~a'." id)))
  nil)

(defun make-uid ()
  (lack.util:generate-random-id))

(defun create-widget-from-config (uid parent &key (widget-store nil))
  (let ((entry (find uid (or widget-store
			     (piggyback-parameters:get-value :widgets :file-only))
		     :test #'string-equal :key #'(lambda (elm) (getf elm :uid)))))
    (if entry
	(create-widget-from-plist entry parent)
	(error "No widget definitio with uid '~a'." uid))))

(defun create-widget-from-plist (plist parent)
  ;; plist (:class :page :uid "A5FT7LM" :icon "assets/floor.png" :description "1st Floor" :size (110 140) :position (250 400))
  (let ((widget-class (get-widget-class-from-keyword (getf plist :class)))
	(plist-initargs nil))
    (unless widget-class
      (error "No Widget class '~a' found." (getf plist :type)))
      ;;(return-from create-widget-from-plist nil))

    (loop for elm on plist by #'cddr
	  unless (eq (car elm) :class)
	    nconcing (list (car elm) (cadr elm)) into collected
	  finally (setf plist-initargs collected))

    (apply #'make-instance widget-class (append (list :parent parent) plist-initargs))))

(defun create-new-widget (type icon top left)
  (let* ((canvas (current-canvas))
	 (widget (create-widget-from-plist (list :class type :icon icon :position (list top left)) canvas)))
    (unless widget
      (log:error "Could not create widget!")
      (return-from create-new-widget nil))

    (setf (widget-config-mode widget) t)

    (push widget
	  (elements canvas))
    (update canvas)))

(defgeneric get-widget-class-from-keyword (keyword))

(defmethod get-widget-class-from-keyword ((keyword t))
  nil)
