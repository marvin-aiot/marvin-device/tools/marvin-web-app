(in-package :marvin-web-app)

(defwidget header-widget ()
  ((parent :initarg :parent
	   :reader parent)
   (height :initarg :height
	   :initform "3em"
	   :reader widget-height)))

(defmethod get-css-classes ((widget header-widget))
  (append '(:navbar :navbar-light :bg-light :fixed-top :flex-md-nowrap :p-0 :shadow)
	  (call-next-method)))

(defmethod render ((widget header-widget))
  (with-html
    (:a :class "navbar-brand col-sm-3 col-md-2 mr-0" :href "#" "Marvin AIoT")
    (:nav :class "my-2 my-md-0 mr-md-3"
	  (:a :id "canvas-edit"
	      :class "p-2 text-dark"
	      :href "#edit"
	      :onclick (ps (if (equal (chain (getprop ($ "#canvas-edit") 0) inner-H-T-M-L) "Unlock Canvas")
			       (progn
				 (chain ($ ".widget-base") (add-class "edit-mode"))
				 (chain ($ ".widget-base") (add-class "draggable-widget"))
				 (chain ($ ".widget-edit-extras") (remove-class "invisible-widget-edit-extras"))

				 ;; Register Mutation Observer
				 (connect-during-config-observer)

				 (setf (chain (getprop ($ "#canvas-edit") 0) inner-H-T-M-L) "Lock Canvas"))
			       (progn
				 ;; @TODO Check that no Widget is in configuration mode
				 ;; Disconnect the Mutation Observer
				 (disconnect-during-config-observer)
				 ;; @TODO Collect the configuration of the canvas and store it on Lisp side.

				 (chain ($ ".widget-base") (remove-class "edit-mode"))
				 (chain ($ ".widget-base") (remove-class "draggable-widget"))
				 (chain ($ ".widget-edit-extras") (add-class "invisible-widget-edit-extras"))
				 (setf (chain (getprop ($ "#canvas-edit") 0) inner-H-T-M-L) "Unlock Canvas"))))
	      "Unlock Canvas")
	  (:a :class "p-2 text-dark"
	      :href "#help"
	      :onclick (make-js-action
			(lambda (&key &allow-other-keys)
			  (show-help)))
	      "Help"))))

(defmethod initialize-instance :after ((this widget-base) &key)
  (define-dependency
      this :before :edit-mode-observer
    (ps (defvar during-config-observer nil)

	(defun connect-during-config-observer ()
	  (let ((config (create
			 attributes t
			 attribute-filter (*array "class")
			 child-list t
			 subtree t
			 character-data nil)))
	    (chain console (log "Connecting 'during-config-observer' observer."))
	    (chain during-config-observer (observe (getprop (chain document (get-elements-by-class-name "application")) 0)  config))))

	(defun disconnect-during-config-observer ()
	  (chain console (log "Disconnecting 'during-config-observer' observer."))
	  (chain during-config-observer (disconnect)))

	(defun make-draggable (widget top left)
	  (setf (chain widget style position) "absolute")
	  (setf (chain widget style top) top)
	  (setf (chain widget style left) left)
	  (chain ($ widget)
		 (draggable (create grid (array 5 5)
				    scroll t
				    helper "original"
				    containment "parent"
				    revert "invalid"
				    ;;stack ".ui-droppable"
				    stop (lisp (weblocks-parenscript:make-inline-js-handler
						:lisp-code ((&key widget-id top left)
							    (log:info widget-id top left)
							    (update-widget widget-id :top top :left left))
						:js-code ((event ui)
							  (chain console (log ui))
							  (create :widget-id (chain widget id)
								  :top (chain ui position top)
								  :left (chain ui position left)))
						:ps-return parenscript:t))
				    )))
	  parenscript:false)

	(setf during-config-observer
	      (new (*mutation-observer
		    (lambda (mutations)
		      (chain mutations
			     (for-each
			      (lambda (mutation)
				;;(chain console (log mutation))
				(cond ((eq (chain mutation type) "childList")
				       (when (and (chain mutation added-nodes)
						(> (chain mutation added-nodes length) 0))
					     ;;(chain console (log (chain mutation added-nodes)))
					     (chain mutation added-nodes
						    (for-each (lambda (el)
								;; Whenever a new Widget is added to the canvas during edit, make it draggable and put it in edit mode
								(when (and (chain el class-list)
									   (chain el class-list (contains "widget-base"))
									   (not (chain el class-list (contains "draggable-widget"))))
								  (chain console (log "Element 'widget-base' added"))
								  ;;(chain console (log mutation))
								  (chain ($ el) (add-class "draggable-widget"))
								  (chain ($ el) (add-class "edit-mode"))
								  (chain ($ ".widget-edit-extras" el) (remove-class "invisible-widget-edit-extras"))
								  (make-draggable el
										  (chain (get-computed-style el) (get-property-value "top"))
										  (chain (get-computed-style el) (get-property-value "left"))))
								nil))))
				       nil)
				      ((eq (chain mutation type) "attributes")
				       (when (and (equal (chain mutation attribute-name) "class")
						  (chain mutation target)
						  (chain (chain mutation target) class-list (contains "widget-base")))
					 (when (and (chain (chain mutation target) class-list (contains "draggable-widget"))
						    (not (chain (chain mutation target) class-list (contains "ui-draggable"))))
					   (make-draggable (chain mutation target)
							   (chain (get-computed-style target) (get-property-value "top"))
							   (chain (get-computed-style target) (get-property-value "left")))))))
				nil))))))))))

(defmethod weblocks/dependencies:get-dependencies ((widget header-widget))
  (list (create-lass-dependency '((.navbar-brand
				   :font-size 1.4rem)))))

(defmethod get-inline-style ((widget header-widget))
  (format nil "height: ~a;~a"
	  (widget-height widget)
	  (call-next-method)))

(defun show-help ())
(defun action-login ())
(defun action-logout ())
(defun user-logged-in-p ())
(defun edit-current-canvas ())

