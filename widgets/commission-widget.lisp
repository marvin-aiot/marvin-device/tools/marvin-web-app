(in-package :marvin-web-app)

#|

 Lists all discovered devices information (device id, device sn, status) and actions (ISS, DMS [reboot], PIS)

 |#

(defwidget device-actions (weblocks.websocket:websocket-widget)
  ((visible :initform nil
	    :accessor is-visible)
   (device :initform nil
	   :accessor device)
   (position :initform '(0 0)
	     :accessor widget-position)
   (dialog :initform '(:main)
	   :accessor action-dialog)
   (args :initform nil
	 :accessor action-args)))

(defgeneric render-dialog (type widget))

(defmethod render-dialog ((type t) widget)
  (log:error "No known widget action '~a'." (action-dialog widget))
  (with-html (:p (format nil "No known widget action '~a'." (action-dialog widget)))))

(defmethod render-dialog ((type (eql :main)) widget)
  (with-html
    (:div
     (:div :class "row"
	   (:div :class "col"
		 (:button :type "button" :class "action-button btn btn-primary"
			  :onclick (make-js-action #'(lambda ()
						       (push :iss (action-dialog widget))
						       (update widget)))
			  "Set ID"))
	   (:div :class "col"
		 (:button :type "button" :class "action-button btn btn-primary"
			  :onclick (make-js-action #'(lambda ()
						       (push :pis (action-dialog widget))
						       (update widget)))
			  "Access Parameters")))
     (:div :class "row"
	   (:div :class "col"
		 (:button :type "button" :class "action-button btn btn-primary"
			  :onclick (make-js-action #'(lambda ()
						       (push :dms (action-dialog widget))
						       (update widget)))
			  "Device Maintenance"))
	   (:div :class "col"
		 (:button :type "button" :class "action-button btn btn-primary"
			  :onclick (make-js-action #'(lambda ()
						       (push :dus (action-dialog widget))
						       (update widget)))
			  "Update Device"))))))

(defmethod render-dialog ((type (eql :iss)) widget)
  (with-html
    (:div :class "m-2 row g-3 align-items-end"
	  (:div :class "col-auto" (:input :type "text" :class "form-control"
					  :id "new-dev-id" :size 18
					  :placeholder "The new Device ID"))
	  (:div :class "col-auto"
		(:button :type "button" :class "btn btn-primary"
		   :onclick (ps* `(,(weblocks-parenscript:make-inline-js-handler
				     :lisp-code ((&key new-id)
						 (communication-protocol:initiate-iss-service (car (device widget))
											      (parse-integer new-id)
											      :source-id 99
											      :callback #'communication-protocol:ignore-callback)
						 (pop (action-dialog widget))
						 (update widget))
				     :js-code ((event)
					       (create :new-id (chain document (get-element-by-id "new-dev-id") value))))
				   event))
		   "Send")))))

(defun pis-callback (widget parameter-name code args)
  (case code
    (:start-response-received
     (let* ((message (getf args :message))
	    (payload (communication-protocol:payload message))
	    (status (cadr (assoc :status payload)))
	    (data (cadr (assoc :data payload))))

       (if (eq status 0)
	   (progn
	     (setf (getf (action-args widget) :parameter-name) parameter-name)
	     (setf (getf (action-args widget) :parameter-value) data)
	     (update widget))
	   (progn
	     ;; @TODO Create a toast to indicate the error
	     (log:error "Got a response '~a' from device ~a." status (communication-protocol:source-id message))))))
    (:start-request-send-failure
     ;; Create an 'error' toast to inform the user
     (log:info "Failed to process PIS.")
     )))

(defmethod render-dialog ((type (eql :pis)) widget)
  (with-html
    (:div
     (:div :class "row-auto"
	   (:input :type "text" :class "form-control" :size 12
		   :id "parameter_name" :placeholder "Parameter name"
		   :value (getf (action-args widget) :parameter-name))
	   (:input :type "text" :class "form-control" :size 12
		   :id "parameter_value" :placeholder "Parameter value"
		   :value (getf (action-args widget) :parameter-value)))
     (:div :class "col-auto"
	   (:button :type "button" :class "action-button btn btn-primary"
		    :onclick (ps* `(,(weblocks-parenscript:make-inline-js-handler
				      :lisp-code ((&key name)
						  (communication-protocol:initiate-pis-service (cadr (device widget))
											       name
											       :write nil
											       :source-id 99
											       :callback (weblocks.websocket:make-ws-action (code &rest args)
													   (pis-callback widget name code args))))
				      ;; @TODO Add some visual feedback to the user while waiting for a response.
				      :js-code ((event)
						(create :name (chain document (get-element-by-id "parameter_name") value))))
				    event))
		    "Read")
	   (:button :type "button" :class "action-button btn btn-primary"
		    :onclick (ps* `(,(weblocks-parenscript:make-inline-js-handler
				      :lisp-code ((&key name value)
						  (communication-protocol:initiate-pis-service (cadr (device widget))
											       name
											       :value value
											       :write t
											       :source-id 99
											       :callback #'communication-protocol:ignore-callback)
						  (pop (action-dialog widget))
						  (update widget))
				      :js-code ((event)
						(create :name (chain document (get-element-by-id "parameter_name") value)
							:value (chain document (get-element-by-id "parameter_value") value))))
				    event))
		    "Write")))))

(defmethod render-dialog ((type (eql :dms)) widget)
  (with-html
    (:div
     (:button :type "button" :class "action-button btn btn-primary"
	      :onclick (make-js-action #'(lambda ()
					   (communication-protocol:initiate-dms-service (cadr (device widget))
											      :reboot
											      :source-id 99
											      :callback #'communication-protocol:ignore-callback)
					   (pop (action-dialog widget))
					   (update widget)))
	      "Normal Reboot")
     (:button :type "button" :class "action-button btn btn-primary"
	      :onclick (make-js-action #'(lambda ()
					   (communication-protocol:initiate-dms-service (cadr (device widget))
											      :maintenance
											      :source-id 99
											      :callback #'communication-protocol:ignore-callback)
					   (pop (action-dialog widget))
					   (update widget)))
	      "Maintenance Reboot"))))

(defmethod render-dialog ((type (eql :dus)) widget)
  (with-html
    (:div
     (:div :class "col-auto"
	   (:label :for  "update_bin_file" "Select a file")
	   (:input :type "file" :class "form-control"
		   :id "update_bin_file"))
     (:button :type "button" :class "action-button btn btn-primary"
	      :onclick (ps* `(,(weblocks-parenscript:make-inline-js-handler
				     :lisp-code ((&key file-name)
						 ;;(communication-protocol:initiate-dus-service (car (device widget))
						;;					      nil
						;;					      :source-id 99
						 ;;					      :callback #'communication-protocol:ignore-callback)
						 (log:info file-name)
						 (pop (action-dialog widget))
						 (update widget))
				     :js-code ((event)
					       (create :file-name (chain document (get-element-by-id "update_bin_file") value))))
				   event))
	      "Update"))))

(defmethod render ((widget device-actions))
  (when (is-visible widget)
    (with-html
      (:div :class "window-top"
	    (:p (format nil "Device ~a" (first (device widget))))))

      (render-dialog (car (action-dialog widget))  widget)

      (with-html
	(:div :class "window-buttons"
	      (:button :type "button" :class "btn btn-warning"
		       :onclick (make-js-action #'(lambda ()
						    (if (eq (car (action-dialog widget)) :main)
							(progn
							  (setf (is-visible widget) nil)
							  (setf (device widget) nil)
							  (setf (widget-position widget) '(0 0)))
							(pop (action-dialog widget)))

						    (update widget)))
		     (if (eq (car (action-dialog widget)) :main)
			 "Close"
			 "Back"))))))

(defmethod weblocks/dependencies:get-dependencies ((widget device-actions))
  (list (create-lass-dependency '((.device-actions
				   :background-color lightgoldenrodyellow
				   :border-style dotted
				   :border-radius 1em
				   :border-width 1px
				   (.window-top
				    :border-style none
				    :border-bottom-style solid
				    :border-top-left-radius 1em
				    :border-top-right-radius 1em
				    :border-bottom-left-radius 0
				    :border-bottom-right-radius 0
				    :border-width 1px)
				   (.window-buttons
				    :border-style none
				    :border-top-style solid
				    :border-bottom-left-radius 1em
				    :border-bottom-right-radius 1em
				    :border-top-left-radius 0
				    :border-top-right-radius 0
				    :border-width 1px)
				   (.action-button
				    :width 10em
				    :height 4em
				    :margin 1em))))))

(defmethod get-inline-style ((widget device-actions))
  (concatenate 'string
	       (format nil "position: absolute; top: ~apx; left: ~apx;"
		       (first (widget-position widget))
		       (second (widget-position widget)))

	       (unless (is-visible widget)
		   "display: none;")

	       (call-next-method)))

(defwidget commission-widget (widget-base)
  ((table :reader table)
   (data :initform nil
	 :accessor devices)
   (action-widget :initform nil
		  :accessor action-widget)))


(defmethod get-widget-class-from-keyword ((keyword (eql :commission)))
  (find-class 'commission-widget))

(defmethod get-css-classes ((widget commission-widget))
  (append '(:mx-auto) (call-next-method)))

(defmethod initialize-instance :after ((widget commission-widget) &key)
  (setf (slot-value widget 'action-widget) (make-instance 'device-actions))
  (setf (slot-value widget 'table)
	(make-instance 'table-widget
		       :model '("Serial Number" "Device ID" "Status")
		       :on-click-action #'(lambda (sn top left)
					    (setf (device (action-widget widget))
						  (find sn (devices widget) :key #'car :test #'string-equal))
					    (setf (widget-position (action-widget widget))
						  (list top left))
					    (setf (is-visible (action-widget widget)) t)
					    (update (action-widget widget))))))

(defmethod render-widget ((this commission-widget))
  (with-html
    (render (table this))
    (render (action-widget this))))

(defun handle-252 (message widget)
  (let ((changed nil)
	(dev (find (communication-protocol:extract-message-value message)
		   (devices widget)
		   :key #'car :test #'string-equal)))
    (if dev
	(progn
	  (setf changed (not (and (eq (second dev) (communication-protocol:source-id message))
				  (eq (third dev) :unknown))))
	  (when changed
	    (setf (second dev) (communication-protocol:source-id message))
	    (setf (third dev) :unknown)))
	(progn
	  (push (list (communication-protocol:extract-message-value message)
		      (communication-protocol:source-id message)
		    :unknown)
		(devices widget))
	  (setf changed t)))

    (when changed
      (setf (table-data (table widget)) (devices widget))
      (update (table widget)))))

(defmethod start-widget ((widget commission-widget))
  ;; Register
  (register-message-interest (weblocks.websocket:make-ws-action
			       (m w)
			       (handle-252 m w))
			     :any
			     252
			     widget))

(defmethod resume-widget ((widget commission-widget))
  ;; Send an RTR to request uo-to-date message 252
  (communication-protocol:send-message (make-instance 'communication-protocol:message
						      :rtr t
						      :source-id 99 ;; @TODO Even the App should have an ID
						      :extended-info 0
						      :message-id 252
						      :mode 0
						      :payload nil)))

(defmethod pause-widget ((widget commission-widget)))

(defmethod stop-widget ((widget commission-widget))
  (unregister-message-interest #'eval-feedback
			       :any
			       252))

(defmethod weblocks.websocket:on-close ((widget commission-widget) &key code reason)
  (declare (ignore code reason))
  (stop-widget widget))
