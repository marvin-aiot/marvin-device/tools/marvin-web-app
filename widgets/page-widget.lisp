(in-package :marvin-web-app)


#|

 A Page Widget is drown as a normal widget and, when clicked, creates a new canvas, inserts all configures widgets into it and pushes it's canvas onto the application stack.
 The configuration :contents holds the uid of other Widgets.

 |#

(defwidget page-widget (widget-base)
  ((icon :initarg :icon
	 :initform ""
	 :reader page-icon)
   (contents :initarg :contents
	     :reader page-contents)))

(defmethod get-css-classes ((widget page-widget))
  (append '()
	  (call-next-method)))

(defmethod render-widget ((widget page-widget))
  (with-html
    (when (widget-description-ontop widget)
      (:div :class "widget-description"
	    (:p (widget-description widget))))

    (:div :class "button-icon"
	  (:img :src (page-icon widget)))

    (unless (widget-description-ontop widget)
      (:div :class "widget-description"
	    (:p (widget-description widget))))))

(defmethod weblocks/dependencies:get-dependencies ((widget page-widget))
  ;;(list (create-lass-dependency '()))
  (call-next-method))

(defmethod get-widget-class-from-keyword ((keyword (eql :page)))
  (find-class 'page-widget))

(defun create-canvas (page-widget)
  (let* ((all-widgets (piggyback-parameters:get-value :widgets :file-only))
	 (canvas (make-instance 'droppable-canvas :parent (weblocks/widgets/root:get)))
	 (widgets (loop for uid in (page-contents page-widget)
			collect (create-widget-from-config uid canvas :widget-store all-widgets))))
    (setf (elements canvas) widgets)

    (loop for widget in widgets
	  do (start-widget widget))

    (push-canvas canvas)))

(defmethod on-widget-clicked ((widget page-widget))
  (create-canvas widget))

