(in-package :marvin-web-app)


#|

 Show a div containing avaliable Widgets to drop into a canvas.
 It floats on the left edge and is normally minimized, until the mouse moves towards that edge.

 When a Widget is dropped into the canvas, a server-side action is triggered to add that new element.
 It then is created with config-mode set to T.

 |#

(defwidget widget-shop (dependency-holder weblocks/widgets/base:widget)
  ((parent :initarg :parent
	   :reader parent)
   (contents :initarg :contents
	     :reader page-contents)))

(defmethod get-css-classes ((widget widget-shop))
  (append '()
	  (call-next-method)))

(defmethod render ((widget widget-shop))
  )

(defmethod initialize-instance :after ((this widget-shop) &key)
  (define-dependency
      this :before :widget-shop-make-droppable
    (ps (defun make-droppable (#| JQuery object |# widget widget-type widget-icon)
	  (chain widget (draggable (create grid (array 5 5)
					   ;;scroll t
					   helper
					   (lisp (weblocks-parenscript:make-inline-js-handler
						  :lisp-code ((&key type icon top left)
							      (log:info type top left)
							      (create-new-widget type icon top left))
						  :js-code ((dummy)
							    (chain console (log ui))
							    (create :type widget-type
								    :icon widget-icon
								    :top (chain (get-computed-style this) (get-property-value "top"))
								    :left (chain (get-computed-style this) (get-property-value "left"))))))

					   containment "document")))))))

(defmethod weblocks/dependencies:get-dependencies ((widget widget-shop))
  ;;(list (create-lass-dependency '()))
  (call-next-method))

