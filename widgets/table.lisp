(in-package :marvin-web-app)

#|

 Draws a table based on the define data model

 |#

(defwidget table-widget (weblocks.websocket:websocket-widget)
  ((model :initarg :model
	  :initform (error "A data model must be provided to a table widget.")
	  :reader table-model)
   (data :initarg :data
	 :initform nil
	 :accessor table-data)
   (onclick :initarg :on-click-action
	    :initform #'(lambda (id top left)
			  (declare (ignore id top left))
			  (log:warn "No implementation for on-click given."))
	    :accessor on-row-clicked)))

(defmethod weblocks/widgets/base:get-html-tag ((widget table-widget))
  :table)

(defmethod render ((widget table-widget))
  (with-html
    (:thead
     (:tr
      (loop for elm in (table-model widget)
	    collect (:th :scope "col" elm))))
    (:tbody
     (loop for row in (table-data widget)
	   collect
	   (:tr :onclick (ps* `(,(weblocks-parenscript:make-inline-js-handler
				:lisp-code ((&key id top left)
					    (funcall (on-row-clicked widget) id top left))
				:js-code ((event id)
					  (create :id id
						  :top (chain event layer-y)
						  :left (chain event layer-x))))
			       event ,(car row)))
		(loop for n from 0 to (1- (length (table-model widget)))
		      collect (:th :scope "row" (nth n row))))))))

(defmethod get-css-classes ((widget table-widget))
  (append '(:table :table-hover :table-striped)
	  (call-next-method)))

(defmethod initialize-instance :after ((this table-widget) &key)
  )

