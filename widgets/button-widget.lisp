(in-package :marvin-web-app)


#|

A Button Widget is drown as a normal widget and, when clicked, executes some action. Can display visual feedback by changing its icon.
 
Feedback is given by Device/Message -> Value <op> Reference
From the information of the Message-ID we derive the extractor of the value
feedback-operation is one of: none, greater than, greater equal, equal, less equal, less than, member of
Any operation can be negated

Action is a PIS request for 'relais_state'

 |#

(defwidget button-widget (widget-base weblocks.websocket:websocket-widget)
  ((feedback-device :initarg :feedback-device
		    :initform 0
		    :reader button-feedback-device)
   (feedback-message :initarg :feedback-message
		     :initform 0
		     :reader button-feedback-message)
   (feedback-operation :initarg :feedback-operation
		       :initform :eq
		       :type (member :none :gt :ge :eq :le :lt :member)
		       :reader button-feedback-operation)
   (feedback-operation-negated :initarg :feedback-operation-negated
			       :initform nil
			       :type boolean
			       :reader button-feedback-operation-negated)
   (feedback-value-ref :initarg :feedback-value-ref
		       :initform 0
		       :reader button-feedback-value-ref)
   (feedback-last-eval :initform nil
		       :accessor button-feedback-last-eval)
   (action-device :initarg :action-device
		  :initform 0
		  :reader button-action-device)
   (action-parameter :initarg :action-parameter
		     :initform nil
		     :reader button-action-parameter)
   (action-parameter-values :initarg :action-parameter-values
			    :initform '(1 0)
			    :reader button-action-parameter-values)))

(defmethod get-css-classes ((widget button-widget))
  (append '()
	  (call-next-method)))

(defmethod render-widget ((widget button-widget))
  (with-html
    (when (widget-description-ontop widget)
      (:div :class "widget-description"
	    (:p (widget-description widget))))

    (:div :class "button-icon"
	  (:img :src (format nil "assets/img/ldpi_power_button_~a.png" (if (button-feedback-last-eval widget) "on" "off"))))

    (unless (widget-description-ontop widget)
      (:div :class "widget-description"
	    (:p (widget-description widget))))))

(defun eval-feedback (value widget)
  (unless (eq (button-feedback-operation widget) :none)
    (let* ((changed nil)
	   (op (case (button-feedback-operation widget)
		 (:gt #'>)
		 (:ge #'>=)
		 (:eq #'eq)
		 (:le #'<=)
		 (:lt #'<)
		 (:member #'member)))
	   (result (if (funcall op value (button-feedback-value-ref widget)) t nil)))
      (when (button-feedback-operation-negated widget)
	(setf result (not result)))

      (unless (eq (button-feedback-last-eval widget) result)
	(setf (button-feedback-last-eval widget) result)
	(setf changed t))

      (when (and changed (widget-active widget))
	(update widget)))))

(defun eval-msg-feedback (message widget)
  (eval-feedback (get-value-from-message message) widget))

(defmethod start-widget ((widget button-widget))
  ;; Register
  (register-message-interest (weblocks.websocket:make-ws-action (m w) (eval-msg-feedback m w))
			     (button-feedback-device widget)
			     (button-feedback-message widget)
			     widget)


  ;; Send an RTR to request uo-to-date message
  (communication-protocol:send-message (make-instance 'communication-protocol:message
						      :rtr t
						      :source-id 99 ;; @TODO Even the App should have an ID
						      :extended-info (button-feedback-device widget)
						      :message-id (button-feedback-message widget)
						      :mode 0
						      :payload nil)))

(defmethod resume-widget ((widget button-widget)))

(defmethod pause-widget ((widget button-widget)))

(defmethod stop-widget ((widget button-widget))
  (unregister-message-interest #'eval-feedback
			       (button-feedback-device widget)
			       (button-feedback-message widget)))

(defmethod weblocks.websocket:on-close ((widget button-widget) &key code reason)
  (declare (ignore code reason))
  (stop-widget widget))

(defun click-cb (widget code args)
  (case code
    (:start-response-received
     (let* ((message (getf args :message))
	    (payload (communication-protocol:payload message))
	 (status (cadr (assoc :status payload)))
	 (data (parse-integer (cadr (assoc :data payload)))))

    (if (eq status 0)
	(eval-feedback data widget)
	(progn
	  ;; @TODO Create a toast to indicate the error
	  (log:error "Got a response '~a' from device ~a." (communication-protocol:source-id message) status)))))
    (:start-request-send-failure
     ;; Create an 'error' toast to inform the user
     (log:info "Failed to process PIS.")
     )))

(defmethod on-widget-clicked ((widget button-widget))
  ;; Start a PIS to toggle the value from (button-action-parameter-values widget) and (button-feedback-last-eval widget)
  (communication-protocol:initiate-pis-service (button-action-device widget)
					        (format nil "~a" (button-action-parameter widget))
					       :value (format nil "~a"
							      (if (button-feedback-last-eval widget)
								  (second (button-action-parameter-values widget))
								  (first (button-action-parameter-values widget))))
					       :write t
					       :source-id 99
					       :callback (weblocks.websocket:make-ws-action
							     (code &rest args)
							   (click-cb widget code args))))

(defmethod weblocks/dependencies:get-dependencies ((widget button-widget))
  ;;(list (create-lass-dependency '()))
  (call-next-method))


(defmethod get-widget-class-from-keyword ((keyword (eql :button)))
  (find-class 'button-widget))


(defmethod render-configuration ((widget button-widget))
  (call-next-method))

