(in-package :marvin-web-app)

(defwidget droppable-canvas (canvas)
  ())

(defmethod get-css-classes ((widget droppable-canvas))
  (call-next-method))

(defmethod initialize-instance :after ((this droppable-canvas) &key)
  (define-dependency
      this :after :add-droppable-hook
    (ps
      (chain *Array (from ($ ".canvas"))
	     (for-each
	      (lambda (element)
		(chain ($ element)
		       (droppable (create accept ".draggable-widget"
					  drop
					  (lambda (event ui)
					    ;; Check if this is already the parent
					    (unless (chain ($ this) (is ($ (chain ui draggable 0 parent-node))))
					      ;;(chain console (log ui))
					      ;;(chain console (log event))
					      (drop-widget ($ this) ui))

					    nil))))
		nil)))))

  ;; This actually came from widget-base, but since Widgets are dynamically loaded, no dependency can be defined by them.
  (define-dependency
      this :before :add-draggable-hook
    (ps (let ((observer (new (*mutation-observer
			      (lambda (mutations)
				(chain mutations
				       (for-each
					(lambda (mutation)
					  ;;(chain console (log mutation))
					  (cond ((eq (chain mutation type) "childList")
						 (when (and (chain mutation added-nodes)
							    (> (chain mutation added-nodes length) 0))
						       ;;(chain console (log (chain mutation added-nodes)))
						   (chain mutation added-nodes
							  (for-each (lambda (el)
								      (when (and (chain el class-list) (chain el class-list (contains "draggable-widget")))
									;; element has class `draggable-widget`
									(chain console (log "Element '.draggable-widget' added"))
									;;(chain console (log mutation))
									(make-draggable el
											(chain (get-computed-style el) (get-property-value "top"))
											(chain (get-computed-style el) (get-property-value "left"))))
								      nil))))
						 nil)
						((eq (chain mutation type) "attributes")
						 (let ((target (chain mutation target)))
						   (when (and target
							      (equal (chain mutation attribute-name) "class")
							      (chain target class-list (contains "widget-base")))
						     (when (and (chain target class-list (contains "draggable-widget"))
								(not (chain target class-list (contains "ui-draggable"))))
						       (make-draggable target
								       (chain (get-computed-style target) (get-property-value "top"))
								       (chain (get-computed-style target) (get-property-value "left"))))

						     (when (and (not (chain target class-list (contains "draggable-widget")))
								(chain target class-list (contains "ui-draggable")))
						       (chain ($ target) (draggable "destroy")))))
						 nil))
					  nil)))))))

	      (config (create
		       attributes t
		       attribute-filter (*array "class")
		       child-list t
		       subtree t
		       character-data nil)))

	  (chain console (log "Registering 'draggable' observer."))
	  (chain observer (observe (getprop (chain document (get-elements-by-class-name "application")) 0)  config)))))
  (define-dependency
      this :after :widget-data
    (ps
      (defun collect-widget-data (widget)
	(chain widget (data :widget-data))))))

(defmethod render ((this droppable-canvas))
  (with-html
    (call-next-method)
    (loop for elm in (elements this)
	  do (render elm))))
