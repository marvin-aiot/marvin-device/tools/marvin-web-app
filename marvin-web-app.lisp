(in-package :marvin-web-app)


;;(weblocks/debug:on)
(defvar *port* 8080)
(defparameter *if* ;;"192...")
                   "localhost")
(weblocks/debug:on)

(defapp marvin-web-app :prefix "/")

(defun push-canvas (canvas)
  (loop for widget in (elements (current-canvas))
	do (pause-widget widget))
  (push canvas (slot-value (weblocks/widgets/root:get) 'stack))
  (loop for widget in (elements (current-canvas))
	do (resume-widget widget))
  (update (weblocks/widgets/root:get)))

(defun pop-canvas ()
  (unless (cdr (stack (weblocks/widgets/root:get)))
    (return-from pop-canvas nil))
  
  (loop for widget in (elements (current-canvas))
	do (pause-widget widget))
  (prog1
      (current-canvas)
    (setf (slot-value (weblocks/widgets/root:get) 'stack) (cdr (stack (weblocks/widgets/root:get))))
    (loop for widget in (elements (current-canvas))
	  do (resume-widget widget))
    (update (weblocks/widgets/root:get))))

(defun populate-initial-canvas (canvas)
  (let ((uids (piggyback-parameters:get-value :initial-canvas :file-only))
	(all-widgets (piggyback-parameters:get-value :widgets :file-only)))

    (setf (elements canvas)
	  (loop for uid in uids
		collect (create-widget-from-config uid canvas :widget-store all-widgets)))

    (loop for widget in (elements canvas)
	  do (start-widget widget))))

(defmethod initialize-instance :after ((this application) &key)
  (setf (slot-value this 'header) (make-instance 'header-widget :parent this))
  (setf (slot-value this 'widget-shop) nil) ;;(make-instance 'widget-shop :parent this))
  (setf (slot-value this 'stack) (list (make-instance 'droppable-canvas :parent this)))
  (setf (slot-value this 'toast-holder) nil) ;;(make-instance 'toaster-widget :parent this))
  (setf (slot-value this 'footer) (make-instance 'footer-widget :parent this)))

(defmethod weblocks/session:init ((app marvin-web-app))
  (declare (ignorable app))
  (let ((application (make-instance 'application)))
    (populate-initial-canvas (car (stack application)))
    (return-from weblocks/session:init application)))

(defmethod render ((app marvin-web-app))
  "Default page rendering template and protocol"
  (with-html
    (render (weblocks/widgets/root:get))))

(defun mapc-directory-tree (fn directory)
  (dolist (entry (cl-fad:list-directory directory))
    (unless (cl-fad:directory-pathname-p entry)
      (funcall fn entry))
    (when (cl-fad:directory-pathname-p entry)
      (mapc-directory-tree fn entry))))

(defun serve-static-file (path)
  (let ((name (pathname-name path))
	(type (pathname-type path))
	(directory (pathname-directory path)))
    (weblocks/server:serve-static-file (format nil "~{~a~^/~}/~a.~a"
					       (member "assets" directory :test #'string-equal)
					       name
					       type)
				       (make-pathname
					:name name
					:type type
					:directory directory)
				       :content-type (format nil "image/~a" type))))

(defun serve-static-files ()
  (mapc-directory-tree #'serve-static-file (merge-pathnames "assets/img/" (asdf:system-source-directory :marvin-web-app))))

(defun start-app ()
  (weblocks/server:stop)
  ;;(weblocks/debug:reset-latest-session)
  
  (weblocks/hooks:on-application-hook-start-weblocks serve-static-files/start ()
						     (weblocks/hooks:call-next-hook)
						     (serve-static-files))

  (weblocks/hooks:on-application-hook-reset-session serve-static-files/reset (session)
						    (log:info session)
						    (weblocks/hooks:call-next-hook)
						    (serve-static-files))

  (piggyback-parameters:set-configuration-file
   (merge-pathnames
    #P"config"
    (asdf:system-source-directory :marvin-web-app)))
  
  (communication-protocol:set-incomming-message-callback #'handle-device-message)

  (unless (ignore-errors
	   (communication-protocol:initialize-communication 4242))
    (log:error "Initialization of the Communication Protocol failed!"))

  (weblocks/server:start :interface *if* :port *port*)
  ;;(weblocks/debug:off)
  (log:config :info)
  
  (trivial-open-browser:open-browser (format nil "http://~a:~a/" *if* *port*)))

