(in-package :marvin-web-app)


#|

 Handle incomming Device messages by setting (communication-protocol:set-incomming-message-callback #'handle-device-message).
 This handle-device-message function triggers every widget, which registered interest on that message.

|#

;; ((message (device fun* )* )* )
(defvar *registered-message-interests* nil)
(defun register-message-interest (fun device-id message-id &rest args)
  "device-id and message-id must be an integer or :any"
  (let ((delegate #'(lambda (message)
		      (apply fun (append (list message) args))))
	(msg-holder (find message-id *registered-message-interests* :key #'car)))
    (if msg-holder
	(let ((dev-holder (find device-id (cdr msg-holder) :key #'car)))
	  (if dev-holder
	      (nconc dev-holder (list delegate))                        ;; Message and Device already in the list
	      (push (list device-id delegate) msg-holder)))             ;; Message already exists, but Device not
	(push (list message-id (list device-id delegate))               ;; Not Message nor Device exists yet
	      *registered-message-interests*))))


(defun unregister-message-interest (fun device-id message-id)
  (let ((msg-holder (find message-id *registered-message-interests* :key #'car)))
    (when msg-holder
	(let ((dev-holder (find device-id (cdr msg-holder) :key #'car)))
	  (when dev-holder
	      (delete fun dev-holder))))))

(defun handle-device-message (message)
  (let ((msg-holder (find (communication-protocol:message-id message) *registered-message-interests* :key #'car)))
    (when msg-holder
      (let ((dev-holder (find (communication-protocol:source-id message) (cdr msg-holder) :key #'car))
	    (any-dev (find :any (cdr msg-holder) :key #'car)))
	(when dev-holder
	  (loop for delegate in (cdr dev-holder)
		do (funcall delegate message)))
	(when any-dev
	  (loop for delegate in (cdr any-dev)
		do (funcall delegate message))))))

  (let ((any-msg (find :any *registered-message-interests* :key #'car)))
    (when any-msg
      (let ((dev-holder (find (communication-protocol:source-id message) (cdr any-msg) :key #'car))
	    (any-dev (find :any (cdr any-msg) :key #'car)))
	(when dev-holder
	  (loop for delegate in (cdr dev-holder)
		do (funcall delegate message)))
	(when any-dev
	  (loop for delegate in (cdr any-dev)
		do (funcall delegate message)))))))

(defun get-value-key-from-message (message)
  (case (communication-protocol:message-id message)
	  (31  :ttl)
	  (208 :offset)
	  (230 :movement)
	  (252 :serial-number)
	  (547 :state)
	  (560 :temperature)
	  (561 :humidity)
	  (562 :luminosity)
	  (563 :air-pressure)
	  (564 :air-dust)
	  (565 :air-quality)
	  (580 :contact)))

(defun get-value-from-message (message)
  (multiple-value-bind (value handledp)
      (communication-protocol:extract-message-value message)
    (unless handledp
      (let* ((key (get-value-key-from-message message)))
	(setf value (cadr (assoc key (communication-protocol:payload message))))
	(case (communication-protocol:message-id message)
	  (560 ;; Temperature
	   (setf value (* (coerce (the fixnum (- (the fixnum value) 27300)) 'float) 0.01)))
	  (561 ;; Humidity
	   (setf value (* (coerce value 'float) 0.0015267176))))))
    (return-from get-value-from-message value)))

(defun create-lass-dependency (lass-code)
  (make-instance 'weblocks-lass:lass-dependency
                 :type :css
                 :css (apply #'lass:compile-and-write lass-code)))

(defun serialize (data)
  (labels ((serialize$ (data stream)
		      (cond ((and (consp data)
				  (keywordp (car data)))
			     (let ((stream2 (list :guard 'parenscript:create)))
			       (iterate:iterate
				 (iterate:for elm on data by #'cddr)
				 (nconc stream2 (list (intern (symbol-name (first elm)))))
				 (serialize$ (second elm) stream2))
			       (assert (eq (car stream) :guard))
			       (nconc stream (list (cdr stream2)))))
			    ((consp data)
			     (let ((stream2 (list :guard 'parenscript:array)))
			       (iterate:iterate
				 (iterate:for elm in data)
				 (serialize$ elm stream2))
			       (assert (eq (car stream) :guard))
			       (nconc stream (list (cdr stream2)))))
			    (t (nconc stream (list data))))
		      (assert (eq (car stream) :guard))
		      (cdr stream)))
    (car (serialize$ data (list :guard)))))


(defclass dependency-holder ()
  ((js-dependencies :initform (list :before (make-hash-table) :after (make-hash-table))
                    :allocation :class
                    :reader get-js-dependencies)))

(defun define-dependency (widget position key ps-code)
    (declare (type (member :before :after) position)
	     (type keyword key)
	     (string ps-code))
    (trivial-object-lock:with-object-lock-held ((slot-value widget 'js-dependencies) :test #'equal)
      (setf (gethash key (getf (slot-value widget 'js-dependencies) position))
	    ps-code)))

(defgeneric inject-dependencies (position widget))
(defmethod inject-dependencies (position (widget dependency-holder))
  (declare (type (member :before :after) position))
  (let ((table (getf (slot-value widget 'js-dependencies) position)))
    (declare (type (or null hash-table) table))
    (unless table
      (return-from inject-dependencies nil))
    (with-html
      (iterate:iterate
	(iterate:for (key value) in-hashtable table)
	(:comment (format nil "JS Dependency: ~a" key))
	(weblocks/js:with-javascript
	    value)))))
